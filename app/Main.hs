{-# LANGUAGE OverloadedStrings #-}

import Data.Text
import Database.SQLite.Simple

selectAllCountries :: Connection -> IO [(Int,Text)]
selectAllCountries conn = query_ conn "SELECT * FROM messages"

main :: IO ()
main = withConnection "ulcoforum.db" selectAllCountries >>= mapM_ print