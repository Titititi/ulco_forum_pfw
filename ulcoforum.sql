BEGIN TRANSACTION;

CREATE DATABASE IF NOT EXISTS ulcoforum;

use ulcoforum;

CREATE TABLE IF NOT EXISTS users
(
    id INT PRIMARY KEY NOT NULL AUTOINCREMENT,
    nom VARCHAR(100)
);

INSERT INTO users VALUES(1,'Jean');
INSERT INTO users VALUES(2,'Michel');
INSERT INTO users VALUES(3,'Michelle');

CREATE TABLE IF NOT EXISTS topics
(
    id INT PRIMARY KEY NOT NULL AUTOINCREMENT,
    nom VARCHAR(100)
);

INSERT INTO topics VALUES(1,'Bretagne');
INSERT INTO topics VALUES(2,'Corons');

CREATE TABLE IF NOT EXISTS messages
(
    id INT PRIMARY KEY NOT NULL,
    id_user INT,
    id_topics INT,
    corpus VARCHAR(100),
    date_last_modified DATE,
    FOREIGN KEY(id_user) REFERENCES uers(id),
    FOREIGN KEY(id_topics) REFERENCES topics(id)
);

INSERT INTO messages VALUES(1, 1, 1, "Tu crois vraiment que la Bretagne est la région où il y a la plus grande densité de bars en France ?");
INSERT INTO messages VALUES(2, 2, 1, "Je ne sais pas, mais si c'est l'cas, on serait bien capable d'être sur le podium à l'échelle mondiale !");
INSERT INTO messages VALUES(3, 1, 2, "T'y crô que les breutons chont cheux avec eul pluch eud'bars ?");
INSERT INTO messages VALUES(4, 1, 2, "Michel dirait bin qu'ouais. Qu'est-che t'in dirô Michelle ?");
INSERT INTO messages VALUES(5, 2, 2, "J'in ché tro rin mi. Aller prin t'verre !");

COMMIT;
